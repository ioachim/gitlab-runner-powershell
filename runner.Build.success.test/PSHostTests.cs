﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Management.Automation;
using System.Collections.Generic;
using gitlab_ci_runner.runner;

namespace runner.Build.success.test
{
    [TestClass]
    public class PSHostTests
    {
        [TestMethod]
        public void TestMethod1()
        {
            var output = new List<string>();
            var ps =PowerShell.Create();
            var settings = new PSInvocationSettings { Host = new RunnerHost(output)};
            ps.AddScript("Write-Host 'Hola, mundo!'");


            ps.Invoke(null, settings);
        }
    }
}
