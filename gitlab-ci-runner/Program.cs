﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using gitlab_ci_runner.conf;
using gitlab_ci_runner.runner;
using gitlab_ci_runner.setup;

namespace gitlab_ci_runner
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Console.InputEncoding = Encoding.Default;
            Console.OutputEncoding = Encoding.Default;
            ServicePointManager.DefaultConnectionLimit = 999;

            if (args.Contains("-sslbypass"))
            {
                RegisterSecureSocketsLayerBypass();
            }

            string directoryName = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            if (directoryName != null && directoryName.Substring(0, 1) == @"\")
            {
                Console.WriteLine("Can't run on UNC Path");
            }
            else
            {
                Console.WriteLine("Starting Gitlab CI Runner for Windows");
                Config.LoadConfig();
                if (Config.IsConfigured())
                {
                    // Load the runner
                    Runner.Run();
                }
                else
                {
                    // Load the setup
                    Setup.Run();
                }
            }
            Console.WriteLine();
            Console.WriteLine("Runner quit. Press any key to exit!");
            Console.ReadKey();
        }

        private static void RegisterSecureSocketsLayerBypass()
        {
            ServicePointManager.ServerCertificateValidationCallback +=
                delegate { return true; // **** Always accept
                };
        }
    }
}