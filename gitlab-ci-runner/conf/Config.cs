﻿using System.IO;
using System.Reflection;
using gitlab_ci_runner.helper;

namespace gitlab_ci_runner.conf
{
    internal class Config
    {
        /// <summary>
        ///     URL to the Gitlab CI coordinator
        /// </summary>
        public static string Url;

        /// <summary>
        ///     Gitlab CI runner auth token
        /// </summary>
        public static string Token;

        /// <summary>
        ///     Configuration Path
        /// </summary>
        private static readonly string ConfPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) +
                                                  @"\runner.cfg";

        /// <summary>
        ///     Load the configuration
        /// </summary>
        public static void LoadConfig()
        {
            if (!File.Exists(ConfPath)) return;

            var ini = new IniFile(ConfPath);
            Url = ini.IniReadValue("main", "url");
            Token = ini.IniReadValue("main", "token");
        }

        /// <summary>
        ///     Save the configuration
        /// </summary>
        public static void SaveConfig()
        {
            if (File.Exists(ConfPath))
                File.Delete(ConfPath);

            var ini = new IniFile(ConfPath);
            ini.IniWriteValue("main", "url", Url);
            ini.IniWriteValue("main", "token", Token);
        }

        /// <summary>
        ///     Is the runner already configured?
        /// </summary>
        /// <returns>true if configured, false if not</returns>
        public static bool IsConfigured()
        {
            return !string.IsNullOrEmpty(Url) && !string.IsNullOrEmpty(Token);
        }
    }
}