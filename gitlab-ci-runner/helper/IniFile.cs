﻿using System.Runtime.InteropServices;
using System.Text;

namespace gitlab_ci_runner.helper
{
    /// <summary>
    ///     .ini File Parser
    ///     (c) http://www.codeproject.com/Articles/1966/An-INI-file-handling-class-using-C
    /// </summary>
    internal class IniFile
    {
        public string Path;

        /// <summary>
        ///     INIFile Constructor.
        /// </summary>
        /// <PARAM name="INIPath">Path to the .ini File</PARAM>
        public IniFile(string iniPath)
        {
            Path = iniPath;
        }

        [DllImport("kernel32")]
        private static extern long WritePrivateProfileString(string section,
            string key, string val, string filePath);

        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string section,
            string key, string def, StringBuilder retVal,
            int size, string filePath);

        /// <summary>
        ///     Write Data to the INI File
        /// </summary>
        /// <PARAM name="Section">Section name</PARAM>
        /// <PARAM name="Key">Key Name</PARAM>
        /// <PARAM name="Value">Value Name</PARAM>
        public void IniWriteValue(string section, string key, string value)
        {
            WritePrivateProfileString(section, key, value, Path);
        }

        /// <summary>
        ///     Read Data Value From the Ini File
        /// </summary>
        /// <PARAM name="Section">Section name</PARAM>
        /// <PARAM name="Key">Key Name</PARAM>
        /// <returns>Value</returns>
        public string IniReadValue(string section, string key)
        {
            var temp = new StringBuilder(255);
            GetPrivateProfileString(section, key, string.Empty, temp, 255, Path);
            return temp.ToString();
        }
    }
}