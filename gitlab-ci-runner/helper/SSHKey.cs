﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading;

namespace gitlab_ci_runner.helper
{
    internal class SSHKey
    {
        /// <summary>
        ///     Get the public key
        /// </summary>
        /// <returns></returns>
        public static string GetPublicKey()
        {
            var publicKeyPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile), ".ssh", "id_rsa.pub");

            return File.Exists(publicKeyPath)
                ? File.ReadAllText(publicKeyPath)
                : null;
        }
    }
}