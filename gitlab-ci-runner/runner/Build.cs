﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using gitlab_ci_runner.api;
using Microsoft.Experimental.IO;
using System.Management.Automation;

namespace gitlab_ci_runner.runner
{
    internal class Build
    {
        /// <summary>
        ///     Command list
        /// </summary>
        private readonly LinkedList<string> _commands;

        /// <summary>
        ///     Command output
        ///     Build internal!
        /// </summary>
        private readonly ConcurrentQueue<string> _outputList;

        /// <summary>
        ///     Project Directory
        /// </summary>
        private readonly string _sProjectDir;

        /// <summary>
        ///     Projects Directory
        /// </summary>
        private readonly string _sProjectsDir = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) +
                                                @"\projects";

        private string _lastPwd;

        private PowerShell _psHost;

        /// <summary>
        ///     Build Infos
        /// </summary>
        public BuildInfo BuildInfo;

        /// <summary>
        ///     Execution State
        /// </summary>
        public State State = State.Waiting;

        /// <summary>
        ///     Constructor
        /// </summary>
        /// <param name="buildInfo">Build Info</param>
        public Build(BuildInfo buildInfo)
        {
            BuildInfo = buildInfo;
            _sProjectDir = _sProjectsDir + @"\" + buildInfo.ProjectNameShortened;
            _commands = new LinkedList<string>();
            _outputList = new ConcurrentQueue<string>();
            Completed = false;

            _psHost = PowerShell.Create();
            
            

            _psHost.Streams.Error.DataAdded += errorHandler;

            var env = _psHost.Runspace.SessionStateProxy.Provider.Get("Environment");
            
        }

        /// <summary>
        ///     Build completed?
        /// </summary>
        public bool Completed { get; private set; }

        /// <summary>
        ///     Command output
        /// </summary>
        public string Output
        {
            get
            {
                string t;
                while (_outputList.TryPeek(out t) && string.IsNullOrEmpty(t))
                {
                    _outputList.TryDequeue(out t);
                }
                return String.Join("\n", _outputList) + "\n";
            }
        }

        /// <summary>
        ///     Command Timeout
        /// </summary>
        public int Timeout
        {
            get { return BuildInfo.timeout; }
        }

        /// <summary>
        ///     Run the Build Job
        /// </summary>
        public void Run()
        {
            State = State.Running;

            try
            {
                // Initialize project dir
                InitProjectDir();
                _psHost.Runspace.SessionStateProxy.Path.SetLocation(_sProjectsDir);

                var plinkPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86), "Putty", "plink.exe");

                if (!File.Exists(plinkPath))
                    throw new FileNotFoundException("PLINK executable not found", plinkPath);

                _psHost.Runspace.SessionStateProxy.SetVariable("Env:GIT_SSH", plinkPath);

                // Add build commands
                foreach (string sCommand in BuildInfo.GetCommands())
                {
                    _commands.AddLast(sCommand);
                }

                foreach(var cmd in _commands)
                {
                    if (!Exec(cmd))
                    {
                        State = State.Failed;
                        break;
                    }
                }

                if (State == State.Running)
                {
                    State = State.Success;
                }
            }
            catch (Exception rex)
            {
                _outputList.Enqueue("");
                _outputList.Enqueue("A runner exception occoured: " + rex.Message);
                _outputList.Enqueue("");
                State = State.Failed;
            }

            Completed = true;
        }

        /// <summary>
        ///     Initialize project dir and checkout repo
        /// </summary>
        private void InitProjectDir()
        {
            // Check if projects directory exists
            if (!Directory.Exists(_sProjectsDir))
            {
                // Create projects directory
                Directory.CreateDirectory(_sProjectsDir);
            }

            // Check if already a git repo
            if (Directory.Exists(_sProjectDir + @"\.git") && BuildInfo.allow_git_fetch)
            {
                // Already a git repo, pull changes
                _commands.AddLast(FetchCmd());
                _commands.AddLast(CheckoutCmd());
            }
            else
            {
                // No git repo, checkout
                if (Directory.Exists(_sProjectDir))
                    DeleteDirectory(_sProjectDir);

                _commands.AddLast(CloneCmd());
                _commands.AddLast(CheckoutCmd());
            }
        }

        /// <summary>
        ///     Execute a command
        /// </summary>
        /// <param name="sCommand">Command to execute</param>
        private bool Exec(string command)
        {
            _psHost.Commands.Clear();
            _psHost.AddScript(command);

            var invokeSettings = new PSInvocationSettings { 

                ErrorActionPreference = ActionPreference.Stop,
                ExposeFlowControlExceptions = false,
                Host = new RunnerHost(_outputList),
            };

            _outputList.Enqueue(command);
            _outputList.Enqueue("\n");

            _psHost.Invoke(null, invokeSettings);

            return !_psHost.HadErrors;
        }

        private void errorHandler(object sender, DataAddedEventArgs e)
        {
            var errors = (PSDataCollection<ErrorRecord>)sender;
            var line = errors[e.Index].Exception.Message;
            if (!string.IsNullOrEmpty(line))
            {
                _outputList.Enqueue(line);
            }
        }

        private void outputHandling(object sender, DataAddingEventArgs ev)
        {
            _outputList.Enqueue(ev.ItemAdded as string);
        }

        /// <summary>
        ///     STDOUT/STDERR Handler
        /// </summary>
        /// <param name="sender">Source process</param>
        /// <param name="outLine">Output Line</param>
        private void outputHandler(object sender, DataAddedEventArgs outLine)
        {
            var output = (PSDataCollection<string>)sender;
            var line = output[outLine.Index];
            if (!String.IsNullOrEmpty(line))
            {
                _outputList.Enqueue(line);
            }
        }

        /// <summary>
        ///     Get the Checkout CMD
        /// </summary>
        /// <returns>Checkout CMD</returns>
        private string CheckoutCmd()
        {
            return string.Format("cd {0} ; git reset --hard ; git checkout", _sProjectDir);
        }

        /// <summary>
        ///     Get the Clone CMD
        /// </summary>
        /// <returns>Clone CMD</returns>
        private string CloneCmd()
        {

            return string.Format("cd {0} ; git clone {1} {2} ; cd {3} ; git checkout {4}",
                _sProjectsDir,
                BuildInfo.repo_url,
                BuildInfo.ProjectNameShortened,
                _sProjectDir,
                BuildInfo.sha);
        }

        /// <summary>
        ///     Get the Fetch CMD
        /// </summary>
        /// <returns>Fetch CMD</returns>
        private string FetchCmd()
        {
            return string.Format("cd {0} ; git reset --hard ; git clean -f ; git fetch", _sProjectDir);
        }

        /// <summary>
        ///     Delete non empty directory tree
        /// </summary>
        private void DeleteDirectory(string targetDir)
        {
            string[] files = Directory.GetFiles(targetDir);
            string[] dirs = Directory.GetDirectories(targetDir);

            foreach (string file in files)
            {
                try
                {
                    File.SetAttributes(file, FileAttributes.Normal);
                    File.Delete(file);
                }
                catch (PathTooLongException)
                {
                    LongPathFile.Delete(file);
                }
            }

            foreach (string dir in dirs)
            {
                // Only recurse into "normal" directories
                if ((File.GetAttributes(dir) & FileAttributes.ReparsePoint) == FileAttributes.ReparsePoint)
                    try
                    {
                        Directory.Delete(dir, false);
                    }
                    catch (PathTooLongException)
                    {
                        LongPathDirectory.Delete(dir);
                    }
                else
                    DeleteDirectory(dir);
            }

            try
            {
                Directory.Delete(targetDir, false);
            }
            catch (PathTooLongException)
            {
                LongPathDirectory.Delete(targetDir);
            }
        }
    }
}