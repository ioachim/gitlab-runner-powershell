﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Management.Automation.Host;
using System.Reflection;
using System.Text;

namespace gitlab_ci_runner.runner
{
    class RunnerHost : PSHost
    {
        RunnerHostUI _ui;
        Guid _instanceId;
        const string PS_HOST_NAME = "GitlabCIRunnerHost";


        public RunnerHost(ConcurrentQueue<string>  output)
        {
            _ui = new RunnerHostUI(output);
            _instanceId = Guid.NewGuid();
        }

        public override System.Globalization.CultureInfo CurrentCulture
        {
            get { return CultureInfo.CurrentCulture; }
        }

        public override System.Globalization.CultureInfo CurrentUICulture
        {
            get { return CultureInfo.CurrentUICulture; }
        }

        public override void EnterNestedPrompt()
        {
            throw new NotImplementedException();
        }

        public override void ExitNestedPrompt()
        {
            throw new NotImplementedException();
        }

        public override Guid InstanceId
        {
            get { return _instanceId; }
        }

        public override string Name
        {
            get { return PS_HOST_NAME; }
        }

        public override void NotifyBeginApplication()
        {
            throw new NotImplementedException();
        }

        public override void NotifyEndApplication()
        {
            throw new NotImplementedException();
        }

        public override void SetShouldExit(int exitCode)
        {
            throw new NotImplementedException();
        }

        public override PSHostUserInterface UI
        {
            get { return _ui; }
        }

        public override Version Version
        {
            get { return Assembly.GetExecutingAssembly().GetName().Version; }
        }
    }
}
