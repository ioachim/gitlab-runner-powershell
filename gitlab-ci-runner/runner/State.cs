﻿namespace gitlab_ci_runner.runner
{
    internal enum State
    {
        Running,
        Failed,
        Success,
        Waiting
    }
}