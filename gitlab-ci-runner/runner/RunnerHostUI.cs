﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Management.Automation;
using System.Management.Automation.Host;
using System.Text;

namespace gitlab_ci_runner.runner
{
    class RunnerHostUI : PSHostUserInterface
    {
        readonly ConcurrentQueue<string> _output;
        readonly PSHostRawUserInterface _rawUI;


        public RunnerHostUI(ConcurrentQueue<string> output)
        {
            _output = output;
            _rawUI = new RunnerRawUI();
        }
        
        public override Dictionary<string, PSObject> Prompt(string caption, string message, Collection<FieldDescription> descriptions)
        {
            throw new NotImplementedException();
        }

        public override int PromptForChoice(string caption, string message, Collection<ChoiceDescription> choices, int defaultChoice)
        {
            throw new NotImplementedException();
        }

        public override PSCredential PromptForCredential(string caption, string message, string userName, string targetName, PSCredentialTypes allowedCredentialTypes, PSCredentialUIOptions options)
        {
            throw new NotImplementedException();
        }

        public override PSCredential PromptForCredential(string caption, string message, string userName, string targetName)
        {
            throw new NotImplementedException();
        }

        public override PSHostRawUserInterface RawUI
        {
            get { return _rawUI; }
        }

        public override string ReadLine()
        {
            throw new NotImplementedException();
        }

        public override System.Security.SecureString ReadLineAsSecureString()
        {
            throw new NotImplementedException();
        }

        public override void Write(ConsoleColor foregroundColor, ConsoleColor backgroundColor, string value)
        {
            _output.Enqueue(value);
        }

        public override void Write(string value)
        {
            _output.Enqueue(value);
        }

        public override void WriteDebugLine(string message)
        {
            this.WriteLine(message);
        }

        public override void WriteErrorLine(string value)
        {
            this.WriteLine(value);
        }

        public override void WriteLine(string value)
        {
            _output.Enqueue(string.Format("{0}]\n", value));
        }

        public override void WriteProgress(long sourceId, ProgressRecord record)
        {
            _output.Enqueue(record.StatusDescription);
        }

        public override void WriteVerboseLine(string message)
        {
            this.WriteLine(message);
        }

        public override void WriteWarningLine(string message)
        {
            this.WriteLine(message);
        }
    }
}
