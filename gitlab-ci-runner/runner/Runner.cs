﻿using System;
using System.Threading;
using gitlab_ci_runner.api;
using gitlab_ci_runner.helper;

namespace gitlab_ci_runner.runner
{
    internal class Runner
    {
        /// <summary>
        ///     Build process
        /// </summary>
        private static Build _build;

        /// <summary>
        ///     Build completed?
        /// </summary>
        public static bool Completed
        {
            get { return Running && _build.Completed; }
        }

        /// <summary>
        ///     Build running?
        /// </summary>
        public static bool Running
        {
            get { return _build != null; }
        }

        /// <summary>
        ///     Start the configured runner
        /// </summary>
        public static void Run()
        {
            Console.WriteLine("* Gitlab CI Runner started");
            Console.WriteLine("* Waiting for builds");
            WaitForBuild();
        }

        /// <summary>
        ///     Wait for an incoming build or update current Build
        /// </summary>
        private static void WaitForBuild()
        {
            while (true)
            {
                if (Completed || Running)
                {
                    // Build is running or completed
                    // Update build
                    UpdateBuild();
                }
                else
                {
                    // Get new build
                    GetBuild();
                }
                Thread.Sleep(5000);
            }
        }

        /// <summary>
        ///     Update the current running build progress
        /// </summary>
        private static void UpdateBuild()
        {
            if (_build.Completed)
            {
                // Build finished
                if (PushBuild())
                {
                    Console.WriteLine("[" + DateTime.Now + "] Completed build " + _build.BuildInfo.id);
                    _build = null;
                }
            }
            else
            {
                // Build is currently running
                PushBuild();
            }
        }

        /// <summary>
        ///     PUSH Build Status to Gitlab CI
        /// </summary>
        /// <returns>true on success, false on fail</returns>
        private static bool PushBuild()
        {
            return Network.PushBuild(_build.BuildInfo.id, _build.State, _build.Output);
        }

        /// <summary>
        ///     Get a new build job
        /// </summary>
        private static void GetBuild()
        {
            BuildInfo binfo = Network.GetBuild();
            if (binfo != null)
            {
                // Create Build Job
                _build = new Build(binfo);
                Console.WriteLine("[" + DateTime.Now + "] Build " + binfo.id + " started...");
                var t = new Thread(_build.Run);
                t.Start();
            }
        }
    }
}