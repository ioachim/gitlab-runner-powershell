﻿using System;
using gitlab_ci_runner.conf;
using gitlab_ci_runner.helper;

namespace gitlab_ci_runner.setup
{
    internal class Setup
    {
        /// <summary>
        ///     Start the Setup
        /// </summary>
        public static void Run()
        {
            Console.WriteLine("This seems to be the first run,");
            Console.WriteLine("please provide the following info to proceed:");
            Console.WriteLine();

            // Read coordinator URL
            String sCoordUrl = "";
            while (sCoordUrl == "")
            {
                Console.WriteLine("Please enter the gitlab-ci coordinator URL (e.g. http(s)://gitlab-ci.org:3000/ )");
                sCoordUrl = Console.ReadLine();
            }
            Config.Url = sCoordUrl;
            Console.WriteLine();

            // Register Runner
            RegisterRunner();
        }

        /// <summary>
        ///     Register the runner with the coordinator
        /// </summary>
        private static void RegisterRunner()
        {
            // Read Token
            string sToken = "";
            while (sToken == "")
            {
                Console.WriteLine("Please enter the gitlab-ci token for this runner:");
                sToken = Console.ReadLine();
            }

            // Register Runner
            string sTok = Network.RegisterRunner(SSHKey.GetPublicKey(), sToken);
            if (sTok != null)
            {
                // Save Config
                Config.Token = sTok;
                Config.SaveConfig();

                Console.WriteLine();
                Console.WriteLine("Runner registered successfully. Feel free to start it!");
            }
            else
            {
                Console.WriteLine();
                Console.WriteLine(
                    "Failed to register this runner. Perhaps your SSH key is invalid or you are having network problems");
            }
        }
    }
}